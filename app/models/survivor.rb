class Survivor < ApplicationRecord
  before_update :flag_as_infected, :if => :contamination_reports_changed?

  has_many :resources
  has_many :items, through: :resources
  accepts_nested_attributes_for :resources, reject_if: :all_blank, allow_destroy: true

  # Name Validation
  validates :name, presence: true
  # Age Validation
  validates :age, numericality: { only_integer: true, greater_than: 0 }, 
    presence: true
  # Gender Validation
  validates :gender, presence: true, length: { maximum: 1 },
    inclusion: { in: %w(M F) }
  # Last Location Validation
  validates :last_location, presence: true, 
    format: { with: /\A([\-0-9.]+)+,([\-0-9.]+)\z/ }

  def new_report
    self.contamination_reports += 1
  end

  # Json Format with relationship
  def as_json(options={})
    if !self.infected?
      super(
        include: { 
          resources: { 
            include: { item: { only: [:id, :name, :points] } },
            only: :quantity } } )
    else
      super()
    end
  end

  private
    def flag_as_infected
      self.infected = true if contamination_reports >= 3
    end
end
