class Item < ApplicationRecord
  has_many :resources
  has_many :survivors, through: :resources
end
