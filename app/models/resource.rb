class Resource < ApplicationRecord
  before_update :delete_resource

  belongs_to :survivor, optional: true
  belongs_to :item

  private
    def delete_resource
      self.destroy if self.quantity_was.eql?(0)
    end
end
