class SurvivorsController < ApplicationController
  before_action :set_survivor, only: [:show, :update_location, :report_survivor]

  # GET /survivors
  def index
    @survivors = Survivor.all
    render json: @survivors
  end

  # GET /survivors/1
  def show
    render json: @survivor
  end

  # POST /survivors
  def create
    @survivor = Survivor.new(survivor_params)

    if @survivor.save
      render json: @survivor, status: :created, location: @survivor
    else
      render json: @survivor.errors, status: :unprocessable_entity
    end
  end

  # PATCH /survivors/1/location
  def update_location
    if @survivor.update(survivor_location_params)
      render json: { "message": "Survivor last location was updated successfully" }
    else
      render json: @survivor.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /survivors/1/report
  def report_survivor
    if !@survivor.infected? && @survivor.update_attribute(:contamination_reports, @survivor.new_report)
      render json: { "message": "Contamination report was sent successfully" }
    else
      render json: @survivor.errors, status: :unprocessable_entity
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_survivor
      @survivor = Survivor.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def survivor_params
      params.require(:survivor).permit(:name, :age, :gender, :last_location,
        resources_attributes: [:item_id, :quantity])
    end
    def survivor_location_params
      params.require(:survivor).permit(:last_location)
    end
end
