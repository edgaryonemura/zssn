class ReportsController < ApplicationController

  # Percentage of infected survivors.
  def infected_survivors
    total = Survivor.count
    infected = Survivor.where(infected: true).count

    render json: { "percentage": (infected.to_f/total.to_f) * 100 }
  end

  # Percentage of non-infected survivors.
  def non_infected_survivors
    total = Survivor.count
    non_infected = Survivor.where(infected: false).count

    render json: { "percentage": (non_infected.to_f/total.to_f) * 100 }
  end

  # Average amount of each kind of resource by survivor (e.g. 5 waters per user)
  def items_per_survivor
    items_average = {}
    survivors = Survivor.where(infected: false).count
    items = Item.all
    items.map do |item|
      items_average[item[:name]] = (
        Resource.where(item_id: item[:id]).sum(:quantity)/survivors.to_f)
    end
    render json: items_average
  end

  # Points lost because of infected survivor.
  def points_lost
    total = 0
    resources = Resource.joins(:survivor).select('item_id','quantity').where(:survivors => {:infected => true})

    resources.each do |resource|
      total += resource.item.points * resource.quantity
    end

    render json: { "points_lost": total }
  end
end
