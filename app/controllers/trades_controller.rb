class TradesController < ApplicationController
  before_action :checks_amount_of_items, only: [:create]
  before_action :count_points, only: [:create]
  before_action :is_infected, only: [:create]

  def create
    # survivor1 -> survivor2
    params[:survivors].first[:trade].each do |s|
      Resource.where(survivor_id: params[:survivors].second[:id])
        .where(:item_id => s[:item_id]).first_or_create.increment!(:quantity, s[:quantity])

      # Decrease Item from Survivor
      item_to_decrease = Resource.where(survivor_id: params[:survivors].first[:id]).find_by_item_id(s[:item_id])
      item_to_decrease.decrement!(:quantity, s[:quantity])
    end

    # survivor2 -> survivor1
    params[:survivors].second[:trade].each do |s|
      Resource.where(survivor_id: params[:survivors].first[:id])
        .where(:item_id => s[:item_id]).first_or_create.increment!(:quantity, s[:quantity])

      item_to_decrease = Resource.where(survivor_id: params[:survivors].second[:id]).find_by_item_id(s[:item_id])
      item_to_decrease.decrement!(:quantity, s[:quantity])
    end

    render json: { "message": "The trade was successfully" }, status: :accepted
  end

  private
    def count_points
      first_resources = items_points(params[:survivors].first[:trade]).inject(0){|sum,x| sum + x }
      second_resources = items_points(params[:survivors].second[:trade]).inject(0){|sum,x| sum + x }
      
      unless first_resources.eql?(second_resources)
        render json: { 
          "error": "Both sides of the trade should offer the same amount of points!",
          "survivors": [
            "first_survivor": "#{first_resources} points",
            "second_survivor": "#{second_resources} points"
          ]
        }, status: :unprocessable_entity
      end
    end

    def checks_amount_of_items
      id = params[:survivors].first[:id]
      params[:survivors].first[:trade].each do |r|
        if Resource.where(survivor_id: id).where(item_id: r[:item_id]).where('quantity >= ?', r[:quantity]).first.nil?
          render json: { 
            "error": "Survivors need to have the items to exchange"
          }, status: :unprocessable_entity
        end
      end
    end

    def items_points(resources)
      resources.map do |r|
        item = Item.select('points').where(id: r[:item_id]).take
        item.points * r[:quantity]
      end
    end
    
    # Check if survivor is infected
    def is_infected
      id = params[:survivors].first[:id]
      if Survivor.where(id: id).where(infected: true).first
        render json: { 
          "error": "Survivors can't trade"
        }, status: :unprocessable_entity
      end
    end
end
