#ZSSN (Zombie Survival Social Network)

##Documentation

**Rails**: 5.0.0  
**sqlite**: 3  

###Getting Started
1. Clone the project to your workspace  
`git clone https://bitbucket.org/edgaryonemura/zssn`  

2. Install all dependencies  
`cd your_workspace/zssn`  
`bundle install`  

3. Run all migrations   
`rails db:migrate`  

4. Add default items to database  
`rails db:seed`  

5. Run the API  
`rails server`  


**List Survivors**
----
  List all survivors
  
* **URL**

  /survivors
  
* **Method:**

  `GET`
  
*  **URL Params**

  None

* **Data Params**

  None


* **Success Response:**

  * **Code:** 200   
    **Content:** 
    
```
  {  
    "id": [integer],  
    "name": [string],  
    "age": [integer],  
    "gender": [string],  
    "last_location": [string],  
    "created_at": [date],  
    "updated_at": [date],  
    "contamination_reports": [integer],  
    "infected": [boolean],  
    "resources": [  
      {  
        "quantity": [integer],  
        "item": {  
          "id": [integer],  
          "name": [string],  
          "points": [integer]  
        }  
      },  
    ]  
  }  
```

**Show Survivor**
----
  Show a single survivor
  
* **URL**

  /survivors/:id
  
* **Method:**

  `GET`
  
*  **URL Params**

  id=[integer]

* **Data Params**

  None


* **Success Response:**

  * **Code:** 200   
    **Content:** 
    
```
  {  
    "id": [integer],  
    "name": [string],  
    "age": [integer],  
    "gender": [string],  
    "last_location": [string],  
    "created_at": [date],  
    "updated_at": [date],  
    "contamination_reports": [integer],  
    "infected": [boolean],  
    "resources": [  
      {  
        "quantity": [integer],  
        "item": {  
          "id": [integer],  
          "name": [string],  
          "points": [integer]  
        }  
      },  
    ]  
  }  
```

**Add Survivor**
----
  Store a single survivor with your resources
  
* **URL**

  /survivors
  
* **Method:**

  `POST`
  
*  **URL Params**

  None

* **Data Params**

  **Example:**
  
```
{  
  "survivor": {  
    "name": [string],  
    "age": [integer],  
    "gender": [string],  
    "last_location": [string],  
    "resources_attributes": [  
      { "item_id": [integer], "quantity": [integer] },  
      { "item_id": [integer], "quantity": [integer] }  
    ]  
  }  
}  
```
  
  **Example:**
  
```
{  
  "survivor": {  
    "name": "Cherry Darling",  
    "age": 27,  
    "gender": "F",  
    "last_location": "-33.13756427847571,-80.156225",  
    "resources_attributes": [  
      { "item_id": 1, "quantity": 1 },  
      { "item_id": 4, "quantity": 2 }  
    ]  
  }  
}  
```


* **Success Response:**

  * **Code:** 201   
    **Content:** 
    
```
  {  
    "id": 1,  
    "name": "Cherry Darling",  
    "age": 27,  
    "gender": "F",  
    "last_location": "-33.13756427847571,-80.156225",  
    "created_at": "2016-10-24T00:10:06.246Z",  
    "updated_at": "2016-10-24T00:10:06.246Z",  
    "contamination_reports": 0,  
    "infected": false,  
    "resources": [  
      {   
        "quantity": 1,
        "item": {
          "id": 1,
          "name": "Water",
          "points": 4
        }
      },  
      {   
        "quantity": 2,
        "item": {
          "id": 4,
          "name": "Ammunition",
          "points": 1
        }  
      }  
    ]  
  }  
```

**Update Survivor Location**
----
  Update Survivor Last Location
  
* **URL**

  /survivors/:id/location
  
* **Method:**

  `PATCH`
  
*  **URL Params**

  **REQUIRED:**
  
  `id=[integer]`
  

* **Data Params**

  **Example:**

```
{
    "last_location": [string],
}
```

* **Success Response:**

  * **Code:** 200   
    **Content:** `{ "message": "Survivor last location was updated successfully" }`
    
**Report Survivor Contamination**
----

  
* **URL**

  /survivors/:id/report
  
* **Method:**

  `PATCH`
  
*  **URL Params**

  **REQUIRED:**
  
  `id=[integer]`
  

* **Data Params**

  None

* **Success Response:**

  * **Code:** 200   
    **Content:** `{ "message": "Contamination report was sent successfully" }`
  
**Trade items**
----

  
* **URL**

  /survivors/trade
  
* **Method:**

  `PATCH`
  
*  **URL Params**

  None
  

* **Data Params**

  **Example:**
  
```
{  
  "survivors": [  
    {  
      "id": [integer],  
      "trade": [{ "item_id": [integer], "quantity": [integer] }]  
    },  
    {  
      "id": [integer],  
      "trade": [{ "item_id": [integer], "quantity": [integer] }]  
    }  
  ]  
}  
```


* **Success Response:**

  * **Code:** 202   
    **Content:** `{ "message": "The trade was successfully" }`


* **Error Response:**

  * **Code:** 422 UNPROCESSABLE ENTRY   
    **Content:**  
    
```
{  
  "error": "Both sides of the trade should offer the same amount of points!",  
  "survivors": [  
    "first\_survivor": [points_value],  
    "second\_survivor": [points_value]  
  ]  
}  
```
 


  OR

  * **Code:** 422 UNPROCESSABLE ENTRY   
    **Content:** `{ "error" : "Survivors need to have the items to exchange" }`


  OR

  * **Code:** 422 UNPROCESSABLE ENTRY   
    **Content:** `{ "error" : "Survivors can't trade" }`

**Percentage of infected survivors**
----

  
* **URL**

  /reports/infected
  
* **Method:**

  `GET`
  
*  **URL Params**

  None
  

* **Data Params**
  
  None


* **Success Response:**

  * **Code:** 200   
    **Content:** `{ "percentage": [integer] }`


**Percentage of non-infected survivors**
----

  
* **URL**

  /reports/non-infected
  
* **Method:**

  `GET`
  
*  **URL Params**

  None
  

* **Data Params**
  
  None


* **Success Response:**

  * **Code:** 200   
    **Content:** `{ "percentage": [integer] }`


  **Average amount of each kind of resource by survivor**
----

  
* **URL**

  /reports/items-per-survivor
  
* **Method:**

  `GET`
  
*  **URL Params**

  None
  

* **Data Params**
  
  None


* **Success Response:**

  * **Code:** 200   
    **Content:**  
```
{
  "Water": [integer],  
  "Food": [integer],  
  "Medication": [integer],  
  "Ammunition": [integer]   
}  
```


  **Points lost because of infected survivor**
----

  
* **URL**

  /reports/points-lost
  
* **Method:**

  `GET`
  
*  **URL Params**

  None
  

* **Data Params**
  
  None


* **Success Response:**

  * **Code:** 200
    **Content:**  `{ "points_lost": [integer] }`

