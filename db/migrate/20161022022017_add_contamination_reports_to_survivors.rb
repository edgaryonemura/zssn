class AddContaminationReportsToSurvivors < ActiveRecord::Migration[5.0]
  def change
    add_column :survivors, :contamination_reports, :integer, default: 0
  end
end
