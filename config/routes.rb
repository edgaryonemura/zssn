Rails.application.routes.draw do
  resources :survivors, only: [:index, :show, :create]
  patch 'survivors/:id/location', to: 'survivors#update_location', as: 'survivor_location'
  patch 'survivors/:id/report', to: 'survivors#report_survivor', as: 'survivor_report'
  patch 'survivors/trade', to: 'trades#create', as: 'trade'

  get 'reports/infected', to: 'reports#infected_survivors', as: 'report_infected'
  get 'reports/non-infected', to: 'reports#non_infected_survivors', as: 'report_non_infected'
  get 'reports/items-per-survivor', to: 'reports#items_per_survivor', as: 'report_items_per_survivor'
  get 'reports/points-lost', to: 'reports#points_lost', as: 'report_points_lost'

end
