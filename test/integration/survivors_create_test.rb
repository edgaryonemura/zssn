require 'test_helper'

class SurvivorsCreateTest < ActionDispatch::IntegrationTest
  test "create survivor with invalid information" do
    assert_no_difference 'Survivor.count' do
      post survivors_path, params: { survivor: { name: " ",
        age: 0, gender: "A", last_location: ",123" } }
    end
    assert_response 422
  end

  test "create survivor with valid information" do
    assert_difference 'Survivor.count',1 do
      post survivors_path, params: { survivor: { name: "Cherry Darling",
        age: 37, gender: "F", last_location: "123456,-654321" } }
    end
    assert_response :success
  end
end
