require 'test_helper'

class FlagSurvivorAsInfectedTest < ActionDispatch::IntegrationTest
  def setup
    @cherry = survivors(:cherry)
  end

  test "survivors should not be marked as infected" do
    2.times do
      patch survivor_report_url(@cherry), params: { survivor: { 
        contamination_reports: @cherry.new_report } }, as: :json
    end
    @cherry.reload
    assert_not_equal true, @cherry.infected
    assert_equal false, @cherry.infected
  end

  test "survivors should be marked as infected" do
    3.times do
      patch survivor_report_url(@cherry), params: { survivor: { 
        contamination_reports: @cherry.new_report } }, as: :json
    end
    @cherry.reload
    assert_equal true, @cherry.infected
  end
end
