require 'test_helper'

class SuvivorsComunicationReportsTest < ActionDispatch::IntegrationTest
  def setup
    @cherry = survivors(:cherry)
  end

  test "valid report contamination" do
    patch survivor_report_path(@cherry)
    @cherry.reload
    assert_equal 1, @cherry.contamination_reports
    assert_response 200
  end

end
