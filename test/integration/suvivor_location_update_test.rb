require 'test_helper'

class SuvivorLocationUpdateTest < ActionDispatch::IntegrationTest
  def setup
    @items = items(:water)
    @survivor = survivors(:cherry)
    @resources = resources(:one)
  end

  test "unsuccessful update" do
    patch survivor_location_path @survivor, 
      params: { survivor: { last_location: 123 } }
    assert_response 422
  end

  test "successful update" do
    location = "-12.5464563,15.646549" 
    patch survivor_location_path @survivor, 
      params: { survivor: { last_location: location } }
    @survivor.reload
    assert_equal location, @survivor.last_location
  end
end
