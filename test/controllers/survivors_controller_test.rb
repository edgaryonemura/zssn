require 'test_helper'

class SurvivorsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @survivor = survivors(:cherry)
  end

  test "should get index" do
    get survivors_url, as: :json
    assert_response :success
  end

  test "should create survivor" do
    assert_difference('Survivor.count') do
      post survivors_url, params: { survivor: { 
        name: @survivor.name,
        age: @survivor.age, 
        gender: @survivor.gender, 
        last_location: @survivor.last_location, 
        resources_attributes: [
          { 
            item_id: @survivor.resources.first.item_id, 
            quantity: @survivor.resources.first.quantity 
          } ] } }, as: :json
    end

    assert_response 201
  end

  test "should show survivor" do
    get survivor_url(@survivor), as: :json
    assert_response :success
  end

  test "should update survivor last location" do
    patch survivor_location_url(@survivor), params: { survivor: { 
      last_location: @survivor.last_location } }, as: :json
    assert_response 200
  end

  test "should update survivor contamination reports" do
    patch survivor_report_url(@survivor), params: { survivor: { 
      contamination_reports: @survivor.new_report } }, as: :json
    assert_response 200
  end
end
